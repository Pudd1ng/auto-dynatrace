provider "aws" {
  region = "eu-west-1"
  shared_credentials_file = "${var.shared_credentials_file}"
  profile = "${var.profile_name}"
}

terraform {
  backend "s3" {
    bucket = "dynatrace-state"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

data "aws_ami" "amazon_linux" {
  most_recent = "true"
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "template_file" "user-data" {
  template = "${file("./user-data.sh")}"
  vars {
      dynatrace_url = "${var.dynatrace_url}"
  }
}

data "aws_availability_zones" "available" {}

resource "aws_default_vpc" "default" {
  tags {
    Name = "Default VPC"
  }
}

resource "aws_default_subnet" "default_euw_subnet" {
      availability_zone = "${data.aws_availability_zones.available.names[0]}"
  tags {
    Name = "Default subnet for eu-west-1a"
  }
}

resource "aws_security_group" "docker-sg" {
  vpc_id = "${aws_default_vpc.default.id}"
  name   = "${var.webapp_name}-web"
}

resource "aws_security_group_rule" "http-3000" {
  security_group_id = "${aws_security_group.docker-sg.id}"
  type              = "ingress"
  from_port         = "3000"
  to_port           = "3000"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "http-8080" {
  security_group_id = "${aws_security_group.docker-sg.id}"
  type              = "ingress"
  from_port         = "8080"
  to_port           = "8080"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-22" {
  security_group_id = "${aws_security_group.docker-sg.id}"
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "out" {
  security_group_id = "${aws_security_group.docker-sg.id}"
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "webapp-dockerserver" {
  ami                    = "${data.aws_ami.amazon_linux.id}"
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.docker-sg.id}"]
  subnet_id              = "${aws_default_subnet.default_euw_subnet.id}"
  user_data              = "${data.template_file.user-data.rendered}"
  count                  = "${var.dockerserver_count}"

  tags {
    "Name" = "${var.webapp_name}-v${count.index}"
  }
  key_name = "dynatrace"
}

output "public_ip" {
  value = "${aws_instance.webapp-dockerserver.*.public_ip}"
}

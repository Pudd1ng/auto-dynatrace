#!/bin/bash
sudo yum update -y
sudo yum install -y docker git maven
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
sudo wget  -O Dynatrace-OneAgent-Linux-1.151.251.sh
"https://${dynatrace_url}.live.dynatrace.com/api/v1/deployment/installer/agent/unix/default/latest?Api-Token=${dynatrace_api_token}=x86&flavor=default"
sudo wget https://ca.dynatrace.com/dt-root.cert.pem ; ( echo 'Content-Type: multipart/signed; protocol="application/x-pkcs7-signature"; micalg="sha-256"; boundary="--SIGNED-INSTALLER"'; echo ; echo ; echo '----SIGNED-INSTALLER' ; cat Dynatrace-OneAgent-Linux-1.151.251.sh ) | openssl cms -verify -CAfile dt-root.cert.pem > /dev/null
sudo /bin/sh Dynatrace-OneAgent-Linux-1.151.251.sh  APP_LOG_CONTENT_ACCESS=1 INFRA_ONLY=0
sudo git clone https://gitlab.com/Pudd1ng/sample-web-app.git
cd sample-web-app
sudo service docker start
wait 60
sudo -b docker-compose up
sudo git clone https://gitlab.com/Pudd1ng/store-webapp-sample.git
cd store-webapp-sample
sudo -b mvn seedstack:run

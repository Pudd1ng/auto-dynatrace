# Dynatrace testing and demo

## Required Tfvars
* shared_credentials_file = Location of your AWS credentials
* profile_name = The name of the profile to use in your AWS credentials file
* server_count = The number of application servers to spin up
* dynatrace_url = The random string given to your instance of dynatrace e.g `fru2469`
* dynatrace_api_token = The api token used to hook up the agent to the dynatrace platform


## Useful information
* The Docker goby app hosts on :3000
* The Java classic app hosts on :8080
* For the classic app sign in with demo/demo
* It takes a few minutes for the agent to reconnect to Dynatrace platform wait about 4 minutes from  `terrafrom apply`
variable "webapp_name" {
  default     = "goby-todo"
  description = "name of deployable webapp"
}

variable "dockerserver_count" {
    default = "1"
    description = "Number of webservers to deploy"
}

variable "shared_credentials_file" {
    description = "location to aws credentuaks"
}

variable "profile_name" {
    description = "profile name in credentials file"
}

variable "dynatrace_url" {
    description = "base part of the url for Dynatrace test licence e.g. fru1234"
}

variable "dynatrace_api_token" {
    description = "Apik tokenb used to hook the agent up to the dynatrace platform"
}